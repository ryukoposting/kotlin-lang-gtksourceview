This is a GtkSourceView lang file for the Kotlin programming language. Adding this to
your /usr/share/gtksourceview-3.0/language-specs folder will allow applications that use the 
GtkSourceView library to do proper syntax highlighting for Kotlin.
